import java.util.Scanner;

public class Algoritmo2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese numero de movimiento");
        int movimiento = scanner.nextInt();
        int[] movimientos = new int[movimiento];
        System.out.println("Ingreso de valores para el tablero:");
        for (int i = 0; i < movimiento; i++) {
            movimientos[i] = scanner.nextInt();
        }
        desplazamientoX(movimientos);
    }

    public static void desplazamientoX(int[] movimientos) {
        // inicio de la x
        int x = 0, y = 0;
        // movimiento
        for (int i = 0; i < movimientos.length - 1; i += 2) {
            // calcula desplazamiento
            x += movimientos[i];
            y += movimientos[i + 1];
            // Asegurarse de que la X no salga del tablero
            x = Math.max(0, Math.min(x, 3));
            y = Math.max(0, Math.min(y, 3));
        }
        // procesa el tablero
        char[][] tabla = new char[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                tabla[i][j] = 'O';
            }
        }
        //se define x como fila y y como columna
        tabla[y][x] = 'X'; // 
        // se imprime el tablero por consola
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(tabla[i][j]);
            }
            System.out.println();
        }
    }
}