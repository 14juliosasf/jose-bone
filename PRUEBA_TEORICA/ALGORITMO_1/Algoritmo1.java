
public class Algoritmo1 {
    public static void main(String[] args) {
        // declaramos variable array para crear un arreglo e inicializamos numeros
        int[] array = { 1, 2, 2, 5, 4, 6, 7, 8, 8, 8 };

        // se declara las variable, el contador maximo va almacenar la cantidad de
        // ocurrencias
        // El contador actual nos va matener el conteo actual de ocurrencias seguidad
        // y la variable numero maximo se va almacenar el numero de ocurrencias mas
        // largar
        int contadorMAx = 0;
        int contadorActual = 1;
        int numeroMax = array[0];

        // este metodo for nos ayda a recorrer el arrglo
        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) { // se verifica si el numero actual es igual al numero anterior
                contadorActual++; // se va contar la ocurrencia
            } else {
                // se verifica si es valor actual es mayor que contador max, se actualiza los
                // valores contador maximo y numero maximo con los valores de numeros actuales y
                // numero anterior (la mas larga)
                if (contadorActual > contadorMAx) {
                    contadorMAx = contadorActual;
                    numeroMax = array[i - 1];
                }
                contadorActual = 1; //se reinicia y contara de nuevo las ocurrencias a 1
            }
        }


        //se verifica si el último número del arreglo
        //tiene mas ocurrencias seguidas que el valor actual de contador maximo

        if (contadorActual > contadorMAx) {
            contadorMAx = contadorActual;
            numeroMax = array[array.length - 1];
        }

        //se imprimira por consola
        System.out.println("Recurrencias: " + contadorMAx);
        System.out.println("Numero: " + numeroMax);
    }

}
