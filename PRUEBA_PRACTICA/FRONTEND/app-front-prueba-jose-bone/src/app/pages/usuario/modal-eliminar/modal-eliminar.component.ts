import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario.model';

@Component({
  selector: 'app-modal-eliminar',
  templateUrl: './modal-eliminar.component.html',
  styleUrls: ['./modal-eliminar.component.css'],
})
export class ModalEliminarComponent {
  @Input() idUsuarioAEliminar!: Usuario;
  @Output() eliminarUsuario = new EventEmitter<number>();

  eliminarUsuarioConfirmado() {
    this.eliminarUsuario.emit(this.idUsuarioAEliminar.id);
  }
}
