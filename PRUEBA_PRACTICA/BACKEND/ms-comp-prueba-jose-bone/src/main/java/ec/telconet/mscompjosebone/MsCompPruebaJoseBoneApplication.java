package ec.telconet.mscompjosebone;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsCompPruebaJoseBoneApplication implements CommandLineRunner {
    /* @Autowired
    private UserService usuarioService; */

    public static void main(String[] args) {
        SpringApplication.run(MsCompPruebaJoseBoneApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //usuarioService.crearUsuarioAdmin(); 
		//Descomentar la linea anterior para el ingreso del primer administrador
    }
}