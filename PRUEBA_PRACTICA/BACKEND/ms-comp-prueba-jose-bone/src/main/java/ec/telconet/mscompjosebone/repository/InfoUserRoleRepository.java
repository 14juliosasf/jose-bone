package ec.telconet.mscompjosebone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.telconet.mscompjosebone.models.InfoUserRol;

public interface InfoUserRoleRepository extends JpaRepository<InfoUserRol, Long>  {
    
}
