-- Tabla admin_usuario
CREATE TABLE admi_usuario (
  id SERIAL PRIMARY KEY,
  usuario VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  nombres VARCHAR(255) NOT NULL,
  apellidos VARCHAR(255) NOT NULL,
  direccion VARCHAR(255),
  telefono VARCHAR(20),
  usuario_creacion VARCHAR(255) NOT NULL,
  fecha_creacion TIMESTAMP NOT NULL,
  foto VARCHAR(1000)
);

-- Tabla admi_rol
CREATE TABLE admi_rol (
  id SERIAL PRIMARY KEY,
  nombre VARCHAR(255) NOT NULL,
  descripcion TEXT
);

-- Tabla info_usuario_rol
CREATE TABLE info_usuario_rol (
  id SERIAL PRIMARY KEY,
  usuario_id INTEGER REFERENCES admi_usuario(id),
  rol_id INTEGER REFERENCES admi_rol(id),
  usuario_creacion VARCHAR(255) NOT NULL,
  fecha_creacion TIMESTAMP NOT NULL,
  usuario_modificacion VARCHAR(255),
  fecha_modificacion TIMESTAMP,
  estado CHAR(1) DEFAULT 'A' CHECK (estado IN ('A', 'I'))
);